<?php

use yii\db\Migration;

/**
 * Class m210801_154529_create_flat
 */
class m210801_154529_create_flat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('flat', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer(),
            'rooms' => $this->integer(),
            'apartment_num' => $this->string(),
            'floor' => $this->integer(),
            'total_area' => $this->float(),
            'price' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-building-complex_id',
            'flat',
            'building_id',
            'building',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-building-complex_id','flat');
        $this->dropTable('flat');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210801_154529_create_flat cannot be reverted.\n";

        return false;
    }
    */
}
