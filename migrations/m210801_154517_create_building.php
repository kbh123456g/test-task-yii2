<?php

use yii\db\Migration;

/**
 * Class m210801_154517_create_building
 */
class m210801_154517_create_building extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('building', [
            'id' => $this->primaryKey(),
            'complex_id' => $this->integer(),
            'max_floor' => $this->integer(),
            'building_name' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-building-complex_id',
            'building',
            'complex_id',
            'apartment_complex',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210801_154517_create_building cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210801_154517_create_building cannot be reverted.\n";

        return false;
    }
    */
}
