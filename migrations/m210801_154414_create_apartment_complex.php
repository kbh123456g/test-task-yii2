<?php

use yii\db\Migration;

/**
 * Class m210801_154414_create_apartment_complex
 */
class m210801_154414_create_apartment_complex extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('apartment_complex', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'district' => $this->string(),
            'address' => $this->string(),
            'city' => $this->string(),
            'first_flat' => $this->integer(),
            'first_flat' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210801_154414_create_apartment_complex cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210801_154414_create_apartment_complex cannot be reverted.\n";

        return false;
    }
    */
}
