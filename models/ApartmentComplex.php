<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apartment_complex".
 *
 * @property int $id
 * @property string $name
 * @property string|null $district
 * @property string $address
 * @property string|null $city
 * @property int|null $first_flat 
 * @property int|null $last_flat 
 *
 * @property Building[] $buildings
 */
class ApartmentComplex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apartment_complex';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'required'],
            [['first_flat', 'last_flat'], 'default', 'value' => null], 
		    [['first_flat', 'last_flat'], 'integer'], 
            [['name', 'district', 'city'], 'string', 'max' => 256],
            [['address'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'district' => 'District',
            'address' => 'Address',
            'city' => 'City',
            'first_flat' => 'First Flat', 
		    'last_flat' => 'Last Flat', 
        ];
    }

    /**
     * Gets query for [[Buildings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBuildings()
    {
        return $this->hasMany(Building::className(), ['complex_id' => 'id']);
    }

    public function createComplex($name, $city)
    {
        $this->name = $name;
        $this->city = $city;
        $this->save();

        return $this->id;
    }
}
