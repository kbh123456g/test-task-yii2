<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flat".
 *
 * @property int $id
 * @property int|null $building_id
 * @property int|null $rooms
 * @property int|null $apartment_num
 * @property int|null $floor
 * @property float|null $total_area
 * @property int|null $price
 *
 * @property Building $building
 */
class Flat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'rooms', 'apartment_num', 'floor', 'price'], 'default', 'value' => null],
		    [['building_id', 'rooms', 'apartment_num', 'floor', 'price'], 'integer'],
            [['total_area'], 'number'],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::className(), 'targetAttribute' => ['building_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_id' => 'Building ID',
            'rooms' => 'Rooms',
            'apartment_num' => 'Apartment Num',
            'floor' => 'Floor',
            'total_area' => 'Total Area',
            'price' => 'Price',
        ];
    }

    /**
     * Gets query for [[Building]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::className(), ['id' => 'building_id']);
        // ->with('complex')
    }

}