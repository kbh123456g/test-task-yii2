<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "building".
 *
 * @property int $id
 * @property int|null $complex_id
 * @property int|null $max_floor
 * @property string|null $building_name
 *
 * @property ApartmentComplex $complex
 * @property Flat[] $flats
 */
class Building extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complex_id', 'max_floor'], 'default', 'value' => null],
            [['complex_id', 'max_floor'], 'integer'],
            [['building_name'], 'string'], 
            [['complex_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApartmentComplex::className(), 'targetAttribute' => ['complex_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'complex_id' => 'Complex ID',
            'max_floor' => 'Max Floor',
            'building_name' => 'Building Name',
        ];
    }

    /**
     * Gets query for [[Complex]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComplex()
    {
        print("function getComplex");
        return $this->hasOne(ApartmentComplex::className(), ['id' => 'complex_id']);
    }

     /** 
    * Gets query for [[Flats]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getFlats() 
   { 
       return $this->hasMany(Flat::className(), ['building_id' => 'id'])->with('flat'); 
   } 

}