<?php

namespace app\controllers;

use app\models\Flat;
use app\models\Building;
use app\models\ApartmentComplex;
use app\controllers\ApartmentComplexController;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Response;
use yii\data\Pagination;
use Yii;

class FlatController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\Flat';

    public function actionSearch()
    {
        $city = \Yii::$app->request->get('city');
        $district = \Yii::$app->request->get('district');
        $address = \Yii::$app->request->get('address');
        $complex = \Yii::$app->request->get('complex');
        $building = \Yii::$app->request->get('building');
        $last_floor = \Yii::$app->request->get('last_floor');
        $flat_floor = \Yii::$app->request->get('flat_floor');
        $rooms = \Yii::$app->request->get('rooms');
        $min_area = \Yii::$app->request->get('min_area');
        $max_area = \Yii::$app->request->get('max_area');
        $min_price = \Yii::$app->request->get('min_price');
        $max_price = \Yii::$app->request->get('max_price');

        $query = new Query;
        $query  ->select([
            'flat.id as id', 
            'apartment_complex.city as city',
            'apartment_complex.district as district',
            'apartment_complex.address as address',
            'apartment_complex.name as complex',
            'building.building_name as b_name',
            'building.max_floor as last_floor',
            'flat.floor as flat_floor',
            'flat.rooms as rooms',
            'flat.total_area as total_area',
            'flat.price as price',
            ]
        )
        ->join('LEFT JOIN', 'building', 'flat.building_id = building.id')
        ->join('LEFT JOIN', 'apartment_complex', 'building.complex_id = apartment_complex.id')
        ->from('flat'); 

        if ($city) {
            $query->andFilterWhere(['like', 'city', $city]); 
        }
        if ($district) {
            $query->andFilterWhere(['like', 'district', $district]); 
        }
        if ($address) {
            $query->andFilterWhere(['like', 'address', $address]); 
        }
        if ($complex) {
            $query->andFilterWhere(['like', 'complex', $complex]); 
        }
        if ($building) {
            $query->andFilterWhere(['like', 'building', $building]); 
        }
        if ($last_floor) {
            $arr = explode(",", $last_floor);
            $query->where(['building.max_floor' => $arr]);
        }
        if ($flat_floor) {
            $arr = explode(",", $flat_floor);
            $query->where(['flat.floor' => $arr]);
        }
        if ($rooms) {
            $arr = explode(",", $rooms);
            $query->where(['flat.rooms' => $arr]);
        }
        if ($min_area) {
            $query->andFilterWhere(['>=', 'flat.total_area', $min_area]); 
        }
        if ($max_area) {
            $query->andFilterWhere(['<=', 'flat.total_area', $max_area]); 
        }
        if ($min_price) {
            $query->andFilterWhere(['>=', 'price', $min_price]); 
        }
        if ($max_price) {
            $query->andFilterWhere(['<=', 'price', $max_price]); 
        }
        
        $pagination = new Pagination([
            'defaultPageSize' => 2,
            'totalCount' => $query->count(),
        ]);

        $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        
        $command = $query->createCommand();
        $data = $command->queryAll();

        //echo $query->createCommand()->sql;
        $result = [];
        foreach ($data as $flat) {
            $result[] = [
            'id'    => $flat['id'],
            'city' => $flat['city'],
            'district' => $flat['district'],
            'address' => $flat['address'],
            'complex' => $flat['complex'],
            'building' => $flat['b_name'],
            'last_floor' => $flat['last_floor'],
            'flat_floor' => $flat['flat_floor'],
            'rooms' => $flat['rooms'],
            'total_area' => $flat['total_area'],
            'price' => $flat['price'],
            ];
        };

        return $result;
    }

    public function actionAdd() 
    { 
        $request = Yii::$app->request;
        $params = Yii::$app->request->getBodyParams();
        // check ap.complex
        $complex = $params['complex'];
        $complex_query = ApartmentComplex::find();
        $complex_query->andFilterWhere(['name' => $complex]);
        $complex_query->andFilterWhere(['city' => $params['city']]);
        $complex_query->limit(1);
        $command = $complex_query->createCommand();
        $data = $command->queryAll();
        if (!$data) {
            // if doesn't exist - return error (or add ap complex? like it's not obligatory fileld?)
            $new_complex = new ApartmentComplex();
            $new_complex->name = $complex;
            $new_complex->address = $params['address'];
            $new_complex->city = $params['city'];
            $new_complex->district = $params['district'];
            $new_complex->first_flat = $params['first_flat'];
            $new_complex->last_flat = $params['last_flat'];
            $new_complex->save();
            $data = $command->queryAll();
        }
        $complex_id = $data[0]['id'];
        // check building in that one ap.complex
        $building_query = Building::find();
        $building_query->andFilterWhere(['complex_id' => $complex_id]);
        $building_query->andFilterWhere(['building_name' => $params['building_name']]);
        $building_query->limit(1);
        $building_command = $building_query->createCommand();
        $building_data = $building_command->queryAll();
        // if doesn't exist - create new
        if (!$building_data) {
            // if doesn't exist - return error (or add ap complex? like it's not obligatory fileld?)
            $new_building = new Building();
            $new_building->complex_id = $complex_id;
            $new_building->max_floor = $params['max_floor'];
            $new_building->building_name = $params['building_name'];
            $new_building->save();
            $building_data = $building_command->queryAll();
        }
        $building_id = $building_data[0]['id'];
        $max_floor = $building_data[0]['max_floor'];
        // check floor - if more than max floor in building - return error
        if ($params['floor'] > $max_floor)
        {
            return "floor error";
        }
        // check flat num in that building
        // if already exists - return error
        $flat_query = Flat::find();
        $flat_query->andFilterWhere(['building_id' => $building_id])
                ->andFilterWhere(['apartment_num' => $params["apartment_num"]]);
        $flat_command = $flat_query->createCommand();
        $flat_data = $flat_command->queryAll();
        // if all passed- add a flat
        if (!$flat_data) {
            $new_flat = new Flat();
            $new_flat->building_id = $building_id;
            $new_flat->rooms = $params['rooms'];
            $new_flat->apartment_num = $params['apartment_num'];
            $new_flat->floor = $params['floor'];
            $new_flat->total_area = $params['total_area'];
            $new_flat->price = $params['price'];
            $new_flat->save();
            $flat_data = $flat_command->queryAll();
            return $new_flat;
        }
          
        return "this flat already in DB"; 
    } 
          
    /** 
     * Updates an existing Flat model. 
     * @param integer $id 
     * @throws NotFoundHttpException if the model cannot be found 
     */ 
    public function actionUpdates($id) 
    { 
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $params = Yii::$app->request->getBodyParams();
        $params['rooms'] ? $model->rooms = $params['rooms'] : $model->rooms;
        $params['price'] ? $model->price = $params['price'] : $model->price;
        $params['apartment_num'] ? $model->apartment_num = $params['apartment_num'] : $model->apartment_num;
        $params['total_area'] ? $model->total_area = $params['total_area'] : $model->total_area;
        
        $building_query = Building::find();
        $building_query->andFilterWhere(['id' => $model->building_id]);
        $command = $building_query->createCommand();
        $building_data = $command->queryAll();
        if ($params['floor'] && $params['floor'] <= $building_data[0]['max_floor']) {
            $model->floor = $params['floor'];
        }

        if ($params['city'] || $params['complex']) {
            // check if this row exists
            $complex_query = ApartmentComplex::find();
            if ($params['city']) {
                $complex_query->andFilterWhere(['city' => $params['city']]);
            }
            if ($params['complex']) {
                $complex_query->andFilterWhere(['name' => $params['complex']]);
            }
            $complex_query->limit(1);
            $command = $complex_query->createCommand();
            $complex_data = $command->queryAll();

            // if complex exists - check building and change bulding_id
            // add building if not exists
            if ($complex_data[0]['id']) {
                // query to select a building with `old` or `new` name for this complex
                $building_query = Building::find();
                $building_query->andFilterWhere(['complex_id' => $complex_data[0]['id']]);
                $building_query->where(['in', 'building_name', [$building_data[0]['building_name'], $params['building_name']]]);
                $command = $building_query->createCommand();
                $data = $command->queryAll();

                if ($data) {
                    if ($params['building_name']) {
                        // update building if requested
                        ($data[0]['building_name'] == $params['building_name'] 
                            ? $model->building_id = $data[0]['id']
                                : $model->building_id = $data[1]['id']);
                    } else {
                        $model->building_id = $data[0]['id'];
                    }
                } else {
                    $new_building = new Building();
                    $new_building->building_name = $building_data[0]['building_name'];
                    $new_building->complex_id = $complex_data[0]['id'];
                    $new_building->save();
                    $model->building_id = $new_building;
                }
            } else {
                // if complex doesn't exist - create new, add building, change building_id
                $new_complex = new ApartmentComplex();
                $new_complex->name = $params['complex'];
                $new_complex->city = $params['city'];
                $new_complex->address = $params['address'];
                $new_complex->save();

                $new_building = new Building();
                $new_building->building_name = $building_data[0]['building_name'];
                $new_building->complex_id = $new_complex->id;
                $new_building->save();

                $model->building_id = $new_building->id;
            }
        }

        $model->save(false);
        
        return $model;
    }
    
    protected function findModel($id)
    {
        if (($model = Flat::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
